#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include <limits>
#include <ctime>
using namespace std;
vector<double> c;
clock_t begin_time;

vector<vector<double>> createMatrix(string line){
    int row, column;
    double value;
    stringstream os(line);
    os>>row>>column;
    vector<vector<double>> matrix(row, vector<double>(column));
    for(int i=0;i<row; i++){
        for(int j=0;j<column; j++){
            os>>value;
            matrix[i][j]=value;
        }
    }
    return matrix;
}

void printHMM(vector<vector<vector<double>>> HMM){
    for(int i=0; i<HMM.size(); i++){
        if(i==0){
            cout<<"A:\n";
        }else if(i==1){
            cout<<"B:\n";
        }else{
            cout<<"pi:\n";
        }
        cout<<HMM[i].size()<<"x"<<HMM[i][0].size()<<"\n";
        for(int j=0; j<HMM[i].size(); j++){
            for(int k=0; k<HMM[i][j].size(); k++){
                cout<<" "<<HMM[i][j][k];
            }
            cout<<"\n";
        }
        cout<<"\n";
    }
}

void printKattis(vector<vector<vector<double>>> HMM){
    for(int i=0; i<HMM.size()-1; i++){
        cout<<HMM[i].size()<<" "<<HMM[i][0].size();
        for(int j=0; j<HMM[i].size(); j++){
            for(int k=0; k<HMM[i][j].size(); k++){
                cout<<" "<<HMM[i][j][k];
            }
        }
        cout<<"\n";
    }
}

vector<vector<double>> createSequence(string line){
    int row, column;
    double value;
    stringstream os(line);
    os>>row;
    vector<vector<double>> matrix(row, vector<double>(1));
    for(int i=0;i<row; i++){
        os>>value;
        matrix[i][0]=value;
    }
    return matrix;
}

vector<vector<double>> matrixMultiplication(vector<vector<double>> matrix1, vector<vector<double>> matrix2){
    vector<vector<double>> result(matrix1.size(), vector<double>(matrix2[0].size()));
    for (int i=0;i<matrix1.size();i++){
        for (int j=0;j<matrix2[0].size();j++){
            for (int k=0;k<matrix1[0].size();k++){
                result[i][j]+=matrix1[i][k]*matrix2[k][j];
            }
        }
    }
    return result;
}

vector<vector<double>> stampForwardAlgorithm(vector<vector<double>> A, vector<vector<double>> B, vector<vector<double>> pi, vector<vector<double>> oT){
    vector<vector<double>> alpha(oT.size(),vector<double>(A.size()));
    //c.clear();
    double sum=0.0;
    //INIT
    int t=0;
    c[t]=0;
    //alpha0i
    for(int i=0; i<A.size(); i++){
        alpha[0][i]=pi[0][i]*B[i][oT[0][0]];
        c[0]=c[0]+alpha[0][i];
    }
    //scale
    c[0]=1/c[0];
    for(int i=0; i<A.size(); i++){
        alpha[0][i]=c[0]*alpha[0][i];
    }
    //alphati
    for(t=1; t<oT.size(); t++){
        c[t]=0;
        for(int i=0; i<A.size(); i++){
            alpha[t][i]=0;
            for(int j=0; j<A.size(); j++){
                alpha[t][i]=alpha[t][i]+alpha[t-1][j]*A[j][i];
                //cout <<" "<< alpha[t].matrix[j];
            }
            alpha[t][i]=alpha[t][i]*B[i][oT[t][0]];
            c[t]=c[t]+alpha[t][i];
        }
        //scale alphati
        c[t]=1/c[t];
        for(int i=0; i<A.size(); i++){
            alpha[t][i]=c[t]*alpha[t][i];
        }
    }
    return alpha;
}


vector<vector<double>> stampBackwardAlgorithm(vector<vector<double>> A, vector<vector<double>> B, vector<vector<double>> pi, vector<vector<double>> oT){
    vector<vector<double>> beta(oT.size(),vector<double>(A.size()));

    int t=oT.size()-1;
    //scalerino last row
    for(int i=0; i<A.size(); i++){
        beta[t][i]=c[t];
    }

    //
    for( int t=oT.size()-2; t >=0 ; t--){
        for(int i=0; i<A.size(); i++){
            beta[t][i]=0;
            for(int j=0; j<A.size(); j++){
                beta[t][i]+=A[i][j]*B[j][oT[t+1][0]]*beta[t+1][j];
            }
            beta[t][i]=c[t]*beta[t][i];
        }
    }
    return beta;
}

vector<vector<vector<double>>> stampEstimateMatrices(vector<vector<double>> A, vector<vector<double>> B, vector<vector<double>> pi, vector<vector<double>> oT){
    vector<vector<vector<double>>> newMatrices(3);
    vector<vector<double>> alpha=stampForwardAlgorithm(A,B,pi,oT);
    vector<vector<double>> beta=stampBackwardAlgorithm(A,B,pi,oT);
    vector<vector<double>> gamma(oT.size(),vector<double>(A.size()));
    vector<vector<vector<double>>> digamma(oT.size(),vector<vector<double>>(A.size(),vector<double>(A.size())));
    double denom;
    for (int i = 0; i < A.size(); i++) {
        denom+=alpha[oT.size()-1][i];

    }

    for(int t=0; t<oT.size()-1; t++){
        for (int i = 0; i < A.size(); i++) {
            gamma[t][i]=0;
            for (int j = 0; j < A.size(); j++) {
                digamma[t][i][j]=alpha[t][i]*A[i][j]*B[j][oT[t+1][0]]*beta[t+1][j]/(denom);
                gamma[t][i]+=digamma[t][i][j];
            }
        }
    }

    for (int i = 0; i < A.size(); i++) {
        gamma[oT.size()-1][i]=alpha[oT.size()-1][i]/(denom);
    }

    //restimate pi
    for (int i = 0; i < A.size(); i++) {
        pi[0][i]=gamma[0][i];
    }

    //restimate a
    double numer;
    for (int i = 0; i < A.size(); i++) {
        for (int j = 0; j < A.size(); j++) {
            numer=0;
            denom=0;
            for(int t=0; t<(oT.size()-1); t++){
                numer+=digamma[t][i][j];
                denom+=gamma[t][i];
            }
            A[i][j]=numer/(denom);
        }
    }

    //restimateb
    for (int i = 0; i < A.size(); i++) {
        for (int j = 0; j < B[0].size(); j++) {
            numer=0;
            denom=0;
            for(int t=0; t<oT.size(); t++){
                if(oT[t][0]==j){
                    numer+=gamma[t][i];
                }
                denom+=gamma[t][i];
            }
            B[i][j]=numer/(denom);
        }
    }
    newMatrices[0]=A;
    newMatrices[1]=B;
    newMatrices[2]=pi;

    return newMatrices;
}

double computeLog(int T){
    double logProb=0;
    for(int i=0; i<T; i++){
        logProb=logProb+log(c[i]);
    }
    logProb=-logProb;
}

vector<vector<vector<double>>> stampIterateMatrices(vector<vector<double>> A, vector<vector<double>> B, vector<vector<double>> pi, vector<vector<double>> oT, int nrobs){
    vector<vector<vector<double>>> matrices(3);
    matrices[0]=A;
    matrices[1]=B;
    matrices[2]=pi;
    oT.resize(nrobs);
    c=vector<double>(oT.size());
    int iters=0, maxIters=5000;
    double oldLogProb, logProb=-INFINITY,e=INFINITY;
    clock_t begin_time = clock();
    do{
        oldLogProb=logProb;
        matrices=stampEstimateMatrices(matrices[0],matrices[1],matrices[2],oT);
        logProb=computeLog(oT.size());
        iters++;
        //e =abs(logProb-oldLogProb);
        //cout<<"oldLogProb: "<<oldLogProb<<", logProb: "<<logProb<<", iters:"<<iters<<"\n";
    }while(iters<maxIters && oldLogProb<logProb);
    cout <<"Time:"<< float( clock () - begin_time ) /  CLOCKS_PER_SEC<<", ";

    cout<<"Observations:"<<nrobs<<", Iterations: "<<iters<<"\n";
    return matrices;
}

vector<int> viterbiAlgorithm(vector<vector<double>> A, vector<vector<double>> B, vector<vector<double>> pi, vector<vector<double>> oT){
    vector<int> mostProbableStateSequence(oT.size());
    vector<vector<double>> T1(oT.size(),vector<double>(A.size()));
    vector<vector<int>> T2(oT.size(),vector<int>(A.size()));

    //INITIALIZATION
    for(int i=0;i<A.size(); i++){
        T1[0][i]=pi[0][i]*B[i][oT[0][0]];
        //cout<<T1[0][i]<<"\n";
    }

    //varje timestep
    for( int t=1; t < oT.size(); t++){
        //varje state
        for( int i=0; i < A.size(); i++){
            //ta reda på maxima
            double max=0.0, test=0.0;
            int argmax=0;
            for (int j = 0; j < A.size(); j++) {
                test=T1[t-1][j]*A[j][i]*B[i][oT[t][0]];
                if(test>max){
                    max=test;
                    T1[t][i]=max;
                    //spara state som gav max
                    T2[t][i]=j;
                }
            }
        }
    }
    //Hitta statesequence med högsta probability
    int argmax=0;
    double max=0.0;
    for(int i=0; i<A.size(); i++){
        if(T1[oT.size()-1][i]>max){
            max=T1[oT.size()-1][i];
            //cout<<i<<"\n";
            argmax=i;
        }
    }

    //Backtracka genom sequence
    int arg=argmax;
    for( int t=oT.size()-1; t >=0 ; t--){
        mostProbableStateSequence[t]=arg;
        arg=T2[t][arg];
    }

    return mostProbableStateSequence;
}
void printDist(vector<vector<double>> corrA,vector<vector<double>> corrB,vector<vector<double>> corrpi, vector<vector<vector<double>>> HMM){
    double distance;
    for(int i=0; i<HMM.size(); i++){
        if(i==0){
            cout<<"A:";
        }else if(i==1){
            cout<<"B:";
        }else{
            cout<<"pi:";
        }
        distance=0;
        for(int j=0; j<HMM[i].size(); j++){
            for(int k=0; k<HMM[i][j].size(); k++){

                if(i==0){
                    distance+=sqrt(pow(corrA[j][k]-HMM[i][j][k],2));
                }else if(i==1){
                    distance+=sqrt(pow(corrB[j][k]-HMM[i][j][k],2));
                }else if(i==2){
                    distance+=sqrt(pow(corrpi[j][k]-HMM[i][j][k],2));
                }
            }
        }
        cout<<"Distance to true matrix: "<<distance<<"\n";
    }
}
int main (int argc, char* argv[]) {
    vector<vector<double>> A,B, pi, oT, corrA, corrB, corrpi;
    vector<vector<vector<double>>> HMM;
    string line;
    int nrobs;

    getline (cin,line);
    A=createMatrix(line);
    getline (cin,line);
    B=createMatrix(line);
    getline (cin,line);
    pi=createMatrix(line);

    getline(cin,line);
    oT=createSequence(line);

    if(argc==2){
        stringstream os(argv[1]);
        os>>nrobs;
    }else{
        nrobs=oT.size();
    }
    cout.precision(20);
    HMM= stampIterateMatrices(A,B,pi,oT, nrobs);
    printKattis(HMM);
    printHMM(HMM);
    return 0;
}
